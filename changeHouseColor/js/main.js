const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
// show nút ấn đổi màu
let colorShow = document.getElementById("colorContainer");
let loadColorPick = () => {
  for (let i = 0; i < colorList.length; i++) {
    colorShow.innerHTML +=
      0 == i
        ? "<button class='color-button " + colorList[i] + " active'></button>"
        : "<button class='color-button " + colorList[i] + "'></button>";
  }
};
loadColorPick();

let colorPicker = document.getElementsByClassName("color-button");
let house = document.getElementById("house");

// đổi màu nhà
for (let index = 0; index < colorPicker.length; index++) {
  colorPicker[index].addEventListener("click", function () {
    changeColor(colorList[index], index);
  });
}

changeColor = (houseColor, btnColor) => {
  for (let index = 0; index < colorPicker.length; index++) {
    colorPicker[index].classList.remove("active");
  }

  colorPicker[btnColor].classList.add("active");
  house.className = "house " + houseColor;
};
